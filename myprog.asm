%include "asm_io.inc"

extern printf

; Based off of first.asm from the linux-ex.zip file

segment .text
global asm_main

asm_main:
	push ebp
	mov ebp, esp

	call print_my_string
	call print_my_string

	mov eax, prompt
	call print_string

	call read_int
	mov [in1], eax

	push eax
	push printstr
	call printf

	mov esp, ebp
	pop ebp
	mov eax, [in1]
	ret

print_my_string:
	mov eax, mystring
	call print_string
	call print_nl

	ret

segment .data
	mystring db   "Hello World!", 0
	prompt   db   "Enter the number: ", 0
	printstr db   "The number: %i", 0x0A, 0

segment .bss
	in1      resd 1
