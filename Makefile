.PHONY:	all

all:	myprog

clean:
	rm -rf myprog *.o

myprog:	myprog.o driver.o asm_io.o
	gcc $(LDFLAGS) -m32 -o $@ $^

%.o: %.asm
	nasm $(ASFLAGS) -DELF_TYPE -f elf -o $@ $^

%.o: %.c
	gcc $(CFLAGS) -c -m32 -o $@ $^